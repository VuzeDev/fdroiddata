Categories:System
License:Apache2
Web Site:
Source Code:https://github.com/SimpleMobileTools/Simple-File-Manager
Issue Tracker:https://github.com/SimpleMobileTools/Simple-File-Manager/issues
Changelog:https://github.com/SimpleMobileTools/Simple-File-Manager/blob/HEAD/CHANGELOG.md

Auto Name:File Manager
Summary:A simple file manager for browsing and editing files and directories
Description:
Can also be used for browsing root files and SDcard content. You can easily
copy, move, delete and share anything you wish.

Contains no ads or unnecessary permissions. It is fully opensource, provides a
Dark theme too.
.

Repo Type:git
Repo:https://github.com/SimpleMobileTools/Simple-File-Manager

Build:1.0,1
    commit=1.0
    subdir=app
    gradle=yes

Build:1.2,2
    commit=1.2
    subdir=app
    gradle=yes

Build:1.3,3
    commit=1.3
    subdir=app
    gradle=yes

Build:1.4,4
    commit=1.4
    subdir=app
    gradle=yes
    prebuild=echo -e "android { lintOptions { disable 'MissingTranslation'\n\ndisable 'ExtraTranslation' }}" >> build.gradle

Build:1.5,5
    commit=1.5
    subdir=app
    gradle=yes
    prebuild=echo -e "android { lintOptions { disable 'MissingTranslation'\n\ndisable 'ExtraTranslation' }}" >> build.gradle

Build:1.6,6
    commit=1.6
    subdir=app
    gradle=yes
    prebuild=echo -e "android { lintOptions { disable 'MissingTranslation'\n\ndisable 'ExtraTranslation' }}" >> build.gradle

Build:1.7,7
    commit=1.7
    subdir=app
    gradle=yes
    prebuild=echo -e "android { lintOptions { disable 'MissingTranslation'\n\ndisable 'ExtraTranslation' }}" >> build.gradle

Build:1.8,8
    commit=1.8
    subdir=app
    gradle=yes
    prebuild=echo -e "android { lintOptions { disable 'MissingTranslation'\n\ndisable 'ExtraTranslation' }}" >> build.gradle

Auto Update Mode:Version %v
Update Check Mode:Tags
Current Version:1.8
Current Version Code:8
